Tiffany Tatum
Age when shot: 22
Eye color: blue
Hair color: blonde
Height: 168cm
Weight: 49kg
Breast size: small
Measurements: 81/61/86
Country: Hungary
Ethnicity: Caucasian

Bio: I love adventure! I dream to live in New York with all of the people and the busy streets. I love the thought of walking through the streets, parks and sitting at outdoor cafes. I also like to go to beaches and lay in the sun.